import { Column, Entity } from "typeorm";
import { BaseEntity } from "../../entities/base.entity";
import { UserDto } from "./user.dto";

@Entity('user')
export class User extends BaseEntity {
    @Column('varchar', { name: 'userName', nullable: true, length: 36 })
    userName: string;

    @Column('varchar', { name: 'password', nullable: true, length: 255 })
    password: string;

    @Column('varchar', { name: 'lastName', nullable: true, length: 36 })
    lastName: string;

    @Column('date', { name: 'birthDate', nullable: true })
    birthDate: Date;

    toDto(): UserDto {
        return {
            id: this.id,
            userName: this.userName,
            lastName: this.lastName,
            birthDate: this.birthDate,
        }
    }

    fromDto(dto: UserDto) {
        this.id = dto.id;
        this.userName = dto.userName;
        this.lastName = dto.lastName;
        this.birthDate = dto.birthDate;
    }

}